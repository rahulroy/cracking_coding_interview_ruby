class String
  def replace_spaces
    new_string = ''
    self.each_char do |char|
      char == ' ' ? (new_string << '%20') : (new_string << char)
    end
    new_string
  end
end
