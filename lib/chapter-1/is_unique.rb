# Avoid monkey patching Ruby's `String` class
class Text
  attr_accessor :string
  def initialize(str)
    @string = str
  end

  # Assuming lowercase & upercase are different
  def has_all_unique_characters?
    string_hash = {}
    string.each_char do |char|
      return false if string_hash.has_key? char
      string_hash[char] = char
    end
    true
  end
end
