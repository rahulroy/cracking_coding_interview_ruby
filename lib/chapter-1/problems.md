# Problems

1. Check if string has all unique characters.
2. Check if one string is the permutation of the other.
3. Replace all spaces in a string with'%20'.
4. Checks if a string is a permutation of a palindrome.
5. There are three types of edits that can be performed on strings: insert, remove or replace a character. WAP to check if they are one edit(or zero edits) away.
6. Write a Program(WAP) to perform basic string compression using the counts of repeated characters. For example, the string aabcccccaaa would become a2blc5a3. Return original string if the "compressed" string would not become smaller than the original string.

7. Given an image represented by an NxN matrix, where each pixel in the image is 4 bytes, WAP to rotate the image by 90 degrees. Also, do this in place.

8. WAP such that if an element in an MxN matrix is 0, its entire row and column are set to 0.

9. Assume you have a method isSubstring which checks if one word is a substring of another. Given two strings, s i and s2, write code to check if s2 is a rotation of si using only one call to isSubstring (e.g.,"waterbottle"is a rota- tion of"erbottlewat")
