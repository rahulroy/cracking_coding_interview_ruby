class PalindromePermutation
  attr_accessor :characters, :letters

  def initialize(string)
    @letters = ('a'..'z')
    @characters = build_char_hash(string)
  end

  def permutation?
    str = ''
    characters.each do |key, value|
      str << key if value % 2 != 0
    end
    str == str.reverse ? true : false
  end

  private

  def build_char_hash(string)
    chars = {}
    string.downcase.each_char do |char|
      if @letters.include? char
        chars[char] ? (chars[char] += 1) : (chars[char] = 1)
      end
    end
    chars
  end
end
