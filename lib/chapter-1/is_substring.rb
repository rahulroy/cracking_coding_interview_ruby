class String
  def is_permutation_of?(string)
    first_hash, second_hash = prepare_hash(self), prepare_hash(string)
    return false if first_hash.length > second_hash.length
    first_hash.each_key do |key|
      return false if !second_hash.include?(key)
    end
    true
  end

  private
  def prepare_hash(string)
    hash = {}
    string.each_char do |char|
      hash[char] = char
    end
    hash
  end
end
