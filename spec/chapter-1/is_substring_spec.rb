require_relative '../spec_helper'
require_relative '../../lib/chapter-1/is_substring'

describe 'Check if a string is a permutation of other string' do
  # True cases
  it 'should return true' do
    expect('this'.is_permutation_of?('this')).to eq true
  end

  it 'should return true' do
    expect('this'.is_permutation_of?('thiss')).to eq true
  end

  it 'should return true' do
    expect('this'.is_permutation_of?('this is true')).to eq true
  end

  it 'should return true' do
    expect('thi'.is_permutation_of?('this')).to eq true
  end

  it 'should return true' do
    expect(''.is_permutation_of?('this')).to eq true
  end

  it 'should return true' do
    expect(''.is_permutation_of?('')).to eq true
  end

  # False cases
  it 'should return false' do
    expect('this is not a permutation'.is_permutation_of?('this')).to eq false
  end

  it 'should return false' do
    expect('this'.is_permutation_of?('')).to eq false
  end
end
