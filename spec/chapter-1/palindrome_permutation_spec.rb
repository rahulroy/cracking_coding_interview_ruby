require_relative '../spec_helper'
require_relative '../../lib/chapter-1/palindrome_permutation'

describe 'check if string has a palindrome permutation' do
  # True cases
  it 'should return true' do
  end

  it 'should return true' do
    expect(PalindromePermutation.new('dad').permutation?).to be true
  end

  it 'should return true' do
    expect(PalindromePermutation.new('civic').permutation?).to be true
  end

  it 'should return true' do
    expect(PalindromePermutation.new('kayak').permutation?).to be true
  end

  it 'should return true' do
    expect(PalindromePermutation.new('madam').permutation?).to be true
  end

  it 'should return true' do
    expect(PalindromePermutation.new('Tact coa').permutation?).to be true
  end

  # False cases
  it 'should return false' do
    expect(PalindromePermutation.new('this is true').permutation?).to be false
  end

  it 'should return false' do
    expect(PalindromePermutation.new('this is false').permutation?).to be false
  end

  it 'should return false' do
    expect(PalindromePermutation.new('this is random text').permutation?).to be false
  end

  it 'should return false' do
    expect(PalindromePermutation.new('random').permutation?).to be false
  end

  it 'should return false' do
    expect(PalindromePermutation.new('drama _ is').permutation?).to be false
  end
end
