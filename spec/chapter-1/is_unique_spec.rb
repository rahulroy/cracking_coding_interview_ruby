require_relative '../spec_helper'
require_relative '../../lib/chapter-1/is_unique'

describe 'test uniqueness of string' do
  def string(str)
    Text.new(str)
  end

  it "should return true" do
    expect(string('this').has_all_unique_characters?).to eq true
  end

  it "should return false" do
    expect(string('thiis').has_all_unique_characters?).to eq false
  end

  it "should return true" do
    expect(string('what is').has_all_unique_characters?).to eq true
  end

  it "should return false" do
    expect(string('what is b').has_all_unique_characters?).to eq false
  end
end
