require_relative '../spec_helper'
require_relative '../../lib/chapter-1/replace_spaces'

describe 'replaces spaces with %20' do
  it 'should replace spaces with %20' do
    expect('this is good'.replace_spaces).to eq 'this%20is%20good'
  end

  it 'should return same string' do
    expect('this_is'.replace_spaces).to eq 'this_is'
  end
end
